using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class Player : MonoBehaviour, IDamageable
{
    public static event Action<WeaponData> _OnChangedWeapon;



    public static Action<WeaponData> OnChangedWeapon
    {
        get
        {
            return _OnChangedWeapon;
        }
        set
        {
            _OnChangedWeapon = value;
        }
    }
    

    public UnityEvent<int> onHpChanged;

    private const int MaxHp = 5;

    

    [Header("Health")] 
    [SerializeField] private int currentHp = MaxHp;

    [SerializeField] private Transform spawnPoint;
    

    #region Attack

    private AttackController _attack;

    #endregion

    private void Start()
    {
        onHpChanged?.Invoke(currentHp);
        _attack = GetComponent<AttackController>();
        
    }

    #region HP

    public void Heal(int _value)
    {
        currentHp += _value;
        onHpChanged?.Invoke(currentHp);
    }
    
    public void DecreaseHp(int _value)
    {
        currentHp -= _value;
        
        if (currentHp <= 0)
        {
            Death();
        }
        onHpChanged?.Invoke(currentHp);
    }

    private void Death()
    {
        currentHp = MaxHp;
        Respawn();
    }

    private void Respawn()
    {
        //rb.velocity = Vector2.zero;
        transform.position = spawnPoint.position;
    }

    public void ApplyDamage(GameObject _source, int _damage)
    {
        throw new NotImplementedException();
    }

    #endregion

    #region Movement



    #endregion
}
