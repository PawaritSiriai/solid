using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackHitBox : MonoBehaviour
{
    public AttackController AttackController;
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (!col.gameObject.CompareTag("Player") && col.TryGetComponent<IDamageable>(out var damageable))
        {
            damageable.ApplyDamage(this.gameObject, AttackController.currentWeapon.Damage);
        }
    }
}
